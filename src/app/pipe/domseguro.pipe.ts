import {Pipe, PipeTransform} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigAPI } from '../models/ApiConfig';

@Pipe({
	name: 'domseguro'
})
export class DomseguroPipe implements PipeTransform {
	
	constructor( private domSanitizer:DomSanitizer){ }
	transform(value:string, url: string): any {
		const apiconfig: ConfigAPI = new ConfigAPI();
		return this.domSanitizer.bypassSecurityTrustResourceUrl( apiconfig.URL + url + value);
	}
}