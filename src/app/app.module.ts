import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpModule, JsonpModule } from '@angular/http';
import { routing, appRoutingProviders } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login.component';
import { ProcesoHomeComponent } from './components/proceso-home.component';
import { DetalleComponent } from './components/detalle.component';
import { IndicadoresComponent } from './components/indicadores.component';
import { AddUserComponent } from './components/add-user.component';
import { ListUsersComponent } from './components/list-users.component';
import { EditUserComponent } from './components/edit-user.component';
import { AddPlantillaComponent } from './components/add-plantilla.component';
import { StructProcComponent } from './components/estructura-proceso.component';
import { AddProcesoComponent } from './components/add-proceso.component';
import { StructIndicComponent } from './components/estructura-indicadores.component';
import { EditPlantillaComponent } from './components/edit-planitlla.component';
import { ProcesoViewComponent } from './components/proceso-view.component';
import { EditProcesoComponent} from './components/edit-proceso.component';
import { RolComponent } from './components/rol.component';
import { ChartModule } from 'angular-highcharts';
import { MenuComponent} from './components/menu.component';
import { HeaderComponent} from './components/header.component';
import { ProcesoComponent } from './components/proceso.component';
import { ChartComponent } from './components/chart.component';
import { ProcesoService } from './services/proceso.service';
import { AutenticationService } from './services/autentication.service';
import { AddProcesoService } from './services/add-proceso.service';
import { RolService } from './services/rol.service';
import { UserService } from './services/user.service';
import { ProcesoViewService } from './services/proceso-view.service';
import { PlantillaService } from './services/plantilla.service';
import { DomseguroPipe} from './pipe/domseguro.pipe';

/* dialogs */
import { DialogUserComponent } from './components/dialog-User.component';
import { DialogPeocesoComponent } from './components/dialog-proceso.component';
import { DialogRamasComponent } from './components/dialog-ramas.component';
import { DialogRolComponent } from './components/dialog-Rol.component';
import { DialogConfirmComponent } from './components/dialog-confirm.component';
import { DialogAlert } from './components/dialog-alert';
import { DialogHijosComponent } from './components/dialog-hijos.component';
import { DialogPermisosRolComponent} from './components/dialog-permisos-rol.component'
import { DialogIndicadoresComponent } from './components/dialog-indicadores';
import { SnakBarInfoComponent } from './components/snak-bar-info.component';
import { TreeIndicadoresComponent } from './components/tree-indicadores.component';



@NgModule({
  exports: [
    CdkTableModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ]
})
export class MaterialModule {}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProcesoHomeComponent,
    DetalleComponent,
    IndicadoresComponent,
    AddUserComponent,
    ListUsersComponent,
    EditUserComponent,
    AddPlantillaComponent,
    StructProcComponent,
    AddProcesoComponent,
    StructIndicComponent,
    EditPlantillaComponent,
    EditProcesoComponent,
    ProcesoViewComponent,
    DialogAlert,
    RolComponent,
    DialogRolComponent,
    DialogConfirmComponent,
    MenuComponent,
    HeaderComponent,
    ProcesoComponent,
    ChartComponent,
    DialogUserComponent,
    DialogPeocesoComponent,
    DialogRamasComponent,
    DialogHijosComponent,
    DomseguroPipe,
    DialogPermisosRolComponent,
    DialogIndicadoresComponent,
    SnakBarInfoComponent,
    TreeIndicadoresComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule, 
    ChartModule,
    MaterialModule
  ],
  providers: [
    ProcesoService,
    AutenticationService,
    AddProcesoService,
    RolService,
    UserService,
    ProcesoViewService,
    PlantillaService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
      DialogAlert,
      DialogRolComponent,
      DialogConfirmComponent,
      DialogUserComponent,
      DialogPeocesoComponent,
      DialogRamasComponent,
      DialogHijosComponent,
      DialogPermisosRolComponent,
      DialogIndicadoresComponent,
      SnakBarInfoComponent,
      TreeIndicadoresComponent
  ],
})
export class AppModule { }
