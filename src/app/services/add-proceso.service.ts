import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Proceso } from '../models/proceso';

import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class AddProcesoService{
	private _configApi: ConfigAPI;

	constructor(private _Http: Http){
		this._configApi = new ConfigAPI();
	}

	ValidarProceso(nombre, idEmpresa): Observable<any>{
		return this._Http.get(`${this._configApi.URL}proceso/ValidarProceso/${nombre}/${idEmpresa}`).map(res => res.json());
	}
}