import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/* models */
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class IndicadorService{
	private _configApi: ConfigAPI;

	constructor(private _Http: Http){
		this._configApi = new ConfigAPI();
	}

	AddIndicador(indicador: any, tipo: number){
		let json = JSON.stringify(indicador);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}indicador/AddIndicador/${tipo}`, params, {headers: headers})
			.map(res => res.json());  
	}

	UpdateIndicador(idIndicador: number, indicador: any, tipo: number){
		let json = JSON.stringify(indicador);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}indicador/UpdateIndicador/${tipo}/${idIndicador}`, params, {headers: headers})
			.map(res => res.json());  
	}

	GetIndicador(idIndicador: number, tipo: number){
		return this._Http.get(`${this._configApi.URL}indicador/GetIndicador/${tipo}/${idIndicador}`).map(res => res.json());
	}

	GetIndicadores(idProceso: number){
		return this._Http.get(`${this._configApi.URL}indicador/GetIndicadores/${idProceso}`).map(res => res.json());
	}

	DeleteIndicador(idIndicador: number){
		return this._Http.delete(`${this._configApi.URL}indicador/DeleteIndicador/${idIndicador}`).map(res => res.json());
	}

	DeleteIndicadores(idProceso: number){
		return this._Http.delete(`${this._configApi.URL}indicador/DeleteIndicadores/${idProceso}`).map(res => res.json());
	}
}