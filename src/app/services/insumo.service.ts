import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/* models */
import { ConfigAPI } from '../models/ApiConfig';
import { Insumo } from '../models/insumo';

@Injectable()

export class InsumoService{
	private _configApi: ConfigAPI;

	constructor(private _Http: Http){
		this._configApi = new ConfigAPI();
	}

	AddInsumo(insumo: Insumo){
		let json = JSON.stringify(insumo);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}insumo/AddInsumo`, params, {headers: headers})
			.map(res => res.json());  
    }
    
    AddRelInsumo(relInsumo: any){
        let json = JSON.stringify(relInsumo);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}insumo/AddInsumo`, params, {headers: headers})
			.map(res => res.json());  
    }

	UpdateInsumo(idInsumo: number, insumo: Insumo){
		let json = JSON.stringify(insumo);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}insumo/UpdateInsumo/${idInsumo}`, params, {headers: headers})
			.map(res => res.json());  
	}

	GetInsumo(idInsumo: number){
		return this._Http.get(`${this._configApi.URL}insumo/GetInsumo/${idInsumo}`).map(res => res.json());
	}

	GetInsumos(idProceso: number, tipo: number){
		return this._Http.get(`${this._configApi.URL}insumo/GetInsumos/${idProceso}/${tipo}`).map(res => res.json());
	}

	DeleteInsumo(idInsumo: number){
		return this._Http.delete(`${this._configApi.URL}insumo/DeleteInsumo/${idInsumo}`).map(res => res.json());
	}

	DeleteInsumos(idsInsumos: number[]){
        let json = JSON.stringify(idsInsumos);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});
		return this._Http.post(`${this._configApi.URL}insumo/DeleteInsumos`, params, {headers: headers}).map(res => res.json());
    }
    
    DeleteRelInsumos(idObjeto: number, tipo: number){
        return this._Http.delete(`${this._configApi.URL}insumo/DeleteRelInsumos/${idObjeto}/${tipo}`).map(res => res.json());
    }

    UploadFile(idInsumo: number, strignFiles: any){
        let json = JSON.stringify(strignFiles);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}insumo/UploadFile/${idInsumo}`, params, {headers: headers})
			.map(res => res.json()); 
    }

    GetFile(file: string){
        return this._Http.get(`${this._configApi.URL}insumo/GetFile/${file}`).map(res => res.json());
    }
}