import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/* models */
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class PlantillaService{
	private _configApi: ConfigAPI;

	constructor(private _Http: Http){
		this._configApi = new ConfigAPI();
	}

	GetTemplates(idEmpresa): Observable<any>{
		return this._Http.get(`${this._configApi.URL}plantilla/GetPlantillas/${idEmpresa}`).map(res => res.json());
	}
}