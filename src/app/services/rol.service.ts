import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Rol } from '../models/rol';
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class RolService{
	private _configApi: ConfigAPI;

	constructor(private _Http: Http){
		this._configApi = new ConfigAPI();
	}

	GetRoles(idEmpresa: number): Observable<any>{
		return this._Http.get(`${this._configApi.URL}rol/GetRoles/${idEmpresa}`).map(res => res.json());
	}

	UpdateRol(rol:Rol, idRol: any): Observable<any>{
		let json = JSON.stringify(rol);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}rol/UpdateRol/${idRol}`, 
			params, {headers: headers}).map(res => res.json());  
	}

	DeleteRol(idRol: number): Observable<any>{
		return this._Http.delete(`${this._configApi.URL}rol/DeleteRol/${idRol}`).map(res => res.json());
	}

	AddRol(rol: Rol): Observable<any>{
		let json = JSON.stringify(rol);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}rol/addRol/`,params, {headers: headers}).map(res => res.json());
	}

	/*==============================
	=            nuevos            =
	==============================*/
	
	GetPermisos(idRol: number): Observable<any>{
		return this._Http.get(`${this._configApi.URL}rol/GetPermisos/${idRol}`).map(res => res.json());
	}
	
	ValidarRol(rol: Rol): Observable<any>{
		let json = JSON.stringify(rol);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}rol/ValidarRol/`,params, {headers: headers}).map(res => res.json());
	}

	GetRol(idRol: number): Observable<any>{
		return this._Http.get(`${this._configApi.URL}rol/GetRol/${idRol}`).map(res => res.json());
	}

	UpdatePermiso(estado: boolean, idRelacion: number): Observable<any>{
		let json = JSON.stringify({estado: estado});
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}rol/UpdatePermisos/${idRelacion}`, 
			params, {headers: headers}).map(res => res.json());  
	}
	/*=====  End of nuevos  ======*/
	
}