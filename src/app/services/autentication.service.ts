import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

/* Models */
import { Autentication } from '../models/autentication';
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class AutenticationService{
	private _configApi: ConfigAPI;
	constructor(private _http: Http){
		this._configApi = new ConfigAPI();
	}

	Autenticate(autentication: Autentication){
		let json = JSON.stringify(autentication);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._http.post(`${this._configApi.URL}acceso/autentication`, params, {headers: headers})
			.map(res => res.json())
	}

}