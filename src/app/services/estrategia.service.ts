import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/* models */
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class EstrategiaService{
	private _configApi: ConfigAPI;

	constructor(private _Http: Http){
		this._configApi = new ConfigAPI();
	}

	AddEstrategia(estrategia: any, tipo: number){
		let json = JSON.stringify(estrategia);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}estrategia/AddEstrategia/${tipo}`, params, {headers: headers})
			.map(res => res.json());  
	}

	UpdateEstrategia(idEstrategia: number, estrategia: any, tipo: number){
		let json = JSON.stringify(estrategia);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}estrategia/UpdateEstrategia/${tipo}/${idEstrategia}`, params, {headers: headers})
			.map(res => res.json());  
	}

	GetEstrategia(idEstrategia: number, tipo: number){
		return this._Http.get(`${this._configApi.URL}estrategia/GetEstrategia/${tipo}/${idEstrategia}`).map(res => res.json());
	}

	GetEstrategias(idIndicador: number, tipo: number){
		return this._Http.get(`${this._configApi.URL}estrategia/GetEstrategias/${tipo}/${idIndicador}`).map(res => res.json());
	}

	DeleteEstrategia(idEstrategia: number){
		return this._Http.delete(`${this._configApi.URL}estrategia/DeleteEstrategia/${idEstrategia}`).map(res => res.json());
	}

	DeleteEstrategias(idIndicador: number){
		return this._Http.delete(`${this._configApi.URL}estrategia/DeleteEstrategias/${idIndicador}`).map(res => res.json());
	}
}