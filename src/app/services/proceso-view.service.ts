import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
// import { Autentication } from '../models/autentication';

/* models */
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class ProcesoViewService{
	private _configApi: ConfigAPI;

	constructor(private _http: Http){
		this._configApi = new ConfigAPI();
	}

	GetProceso(idProceso, idEmpresa): Observable<any>{
		return this._http.get(`${this._configApi.URL}getProceso/${idProceso}`).map(res => res.json());
	}

	GetProcPrin(idProceso,idEmpresa): Observable<any>{
		return this._http.get(`${this._configApi.URL}getProcPrin/${idProceso}`).map(res => res.json());
	}

}