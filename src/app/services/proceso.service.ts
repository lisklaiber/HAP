import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
// import { Autentication } from '../models/autentication';

/* models */
import { ConfigAPI } from '../models/ApiConfig';
import { Level } from '../models/level';
import { Serial } from '../models/serial';

@Injectable()

export class ProcesoService{
	private _configApi: ConfigAPI;

	constructor(private _http: Http){
		this._configApi = new ConfigAPI();
	}

	GetProcesosBase(idEmpresa: number): Observable<any>{
		return this._http.get(`${this._configApi.URL}proceso/GetProcesosBase/${idEmpresa}`).map(res => res.json());
	}

	GetIdsProcesos(idProcesoBase: number):Observable<any>{
		return this._http.get(`${this._configApi.URL}proceso/GetIdsProcesos/${idProcesoBase}`).map(res => res.json());
	}

	GetHijos(idProceso: number){
		return this._http.get(`${this._configApi.URL}level/GetHijos/${idProceso}`).map(res => res.json());
	}

	GetSerials(idProceso: number, tipo: number){
		return this._http.get(`${this._configApi.URL}serial/GetSerials/${idProceso}/${tipo}`).map(res => res.json());
	}

	GetProceso(idProceso: number): Observable<any>{
		return this._http.get(`${this._configApi.URL}proceso/GetDetailProceso/${idProceso}`).map(res => res.json());
	}
	
	GetEvento(idEvento: number): Observable<any>{
		return this._http.get(`${this._configApi.URL}evento/GetEvento/${idEvento}`).map(res => res.json());
	}

	GetDecision(idDecision: number): Observable<any>{
		return this._http.get(`${this._configApi.URL}decision/GetDecision/${idDecision}`).map(res => res.json());
	}

	GetResponsable(idProceso: number){
		return this._http.get(`${this._configApi.URL}proceso/GetResponsable/${idProceso}`).map(res => res.json());
	}

	GetPendientes(idProceso: number){
		return this._http.get(`${this._configApi.URL}proceso/GetPendientes/${idProceso}`).map(res => res.json());
	}

	AddProceso(proceso: any, tipo: number){
		let json: string;
		let tipoURL: string = "";

		if(proceso.fechaIni){
			proceso.fechaIni = `${proceso.fechaIni.getFullYear()}-${proceso.fechaIni.getMonth()}-${proceso.fechaIni.getDate()} ${proceso.fechaIni.getHours()}:${proceso.fechaIni.getMinutes()}:${proceso.fechaIni.getSeconds()}`;
			//proceso.fechaIni = proceso.fechaIni.replace('Z', '');
			proceso.fechaFin = `${proceso.fechaFin.getFullYear()}-${proceso.fechaFin.getMonth()}-${proceso.fechaFin.getDate()} ${proceso.fechaFin.getHours()}:${proceso.fechaFin.getMinutes()}:${proceso.fechaFin.getSeconds()}`;
			//proceso.fechaFin = proceso.fechaFin.replace('Z', '');
		}
		
		if ( tipo === 1 ){
			tipoURL = 'proceso/AddProceso';
		} else if( tipo === 2 ){
			tipoURL = 'evento/AddEvento';
		}
		else{
			tipoURL = 'decision/AddDecision';
		}
			
		json = JSON.stringify(proceso);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._http.post(`${this._configApi.URL}${tipoURL}`,params, {headers: headers}).map(res => res.json());
	}

	UpdateProceso(idProceso: number, proceso: any, tipo: number){
		let json: string;
		let tipoURL: string = "";
		let fechaIni: Date | string;
		let fechaFin: Date | string;
		if(!(typeof proceso.fechaIni === 'undefined')){
			fechaIni = proceso.fechaIni;
			fechaFin = proceso.fechaFin;
			if(!(typeof fechaIni === "string")){
				proceso.fechaIni = `${proceso.fechaIni.getFullYear()}-${proceso.fechaIni.getMonth()}-${proceso.fechaIni.getDate()} ${proceso.fechaIni.getHours()}:${proceso.fechaIni.getMinutes()}:${proceso.fechaIni.getSeconds()}`;
				//proceso.fechaIni = proceso.fechaIni.replace('Z', '');
				
				//proceso.fechaFin = proceso.fechaFin.replace('Z', '');
			}
			if(!(typeof fechaFin === "string")){
				proceso.fechaFin = `${proceso.fechaFin.getFullYear()}-${proceso.fechaFin.getMonth()}-${proceso.fechaFin.getDate()} ${proceso.fechaFin.getHours()}:${proceso.fechaFin.getMinutes()}:${proceso.fechaFin.getSeconds()}`;
			}
		}
		
		if ( tipo === 1 ){
			tipoURL = 'proceso/UpdateProceso';
		} else if( tipo === 2 ){
			tipoURL = 'evento/UpdateEvento';
		}
		else{
			tipoURL = 'decision/UpdateDecision';
		}
		
		json = JSON.stringify(proceso);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._http.put(`${this._configApi.URL}${tipoURL}/${idProceso}`, params, {headers: headers}).map(res => res.json());
	}

	DeleteProceso(idProceso: any, tipo: number): Observable<any>{
		let tipoURL: string = "";
		if ( tipo === 1 ){
			tipoURL = 'proceso/DeleteProceso';
		} else if( tipo === 2 ){
			tipoURL = 'evento/DeleteEvento';
		}
		else{
			tipoURL = 'decision/DeleteDecision';
		}

		return this._http.delete(`${this._configApi.URL}${tipoURL}/${idProceso}`).map(res => res.json());
	}

	AddLevel(level:Level){
		let json = JSON.stringify(level);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._http.post(`${this._configApi.URL}level/AddLevel`, params, { headers: headers }).map(res => res.json());
	}
	
	AddSerial(serial: Serial){
		let json = JSON.stringify(serial);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._http.post(`${this._configApi.URL}serial/AddSerial`, params, { headers: headers }).map(res => res.json());
	}

}