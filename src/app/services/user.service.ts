import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { ConfigAPI } from '../models/ApiConfig';

@Injectable()

export class UserService {
	private _configApi: ConfigAPI;

	constructor(
		private _Http: Http
	){
		this._configApi = new ConfigAPI();
	}

	validarCorreo(correo: string ): Observable<any>{
		let json = JSON.stringify({correo: correo});
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}usuario/ValidarCorreo`, params, {headers: headers})
		.map( res => res.json() );
	}

	GetUsers(idEmpresa: number): Observable<any>{
		return this._Http.get(`${this._configApi.URL}usuario/GetUsuarios/${idEmpresa}`).map(res => res.json());
	}

/*==============================
=            nuevos            =
==============================*/
	GetFoto(foto: string): Observable<any>{
		return this._Http.get(`${this._configApi.URL}usuario/GetFoto/${foto}`).map(res => res.json());
	}

	GetUser(idUsuario: number): Observable<any>{
		return this._Http.get(`${this._configApi.URL}usuario/GetUsuario/${idUsuario}`).map(res => res.json());
	}

	GetProceso(idUsuario: number): Observable<any>{
		return this._Http.get(`${this._configApi.URL}usuario/GetProcesos/${idUsuario}`).map(res => res.json());
	}

	AddUser(formData: FormData): Observable<any>{
		let headers = new Headers({'enctype': 'multipart/form-data'});
		return this._Http.post(`${this._configApi.URL}usuario/AddUsuario/`, formData, {headers: headers}).map(res => res.json());
	}

	AddRol(user: User): Observable<any>{
		let json = JSON.stringify(user);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}usuario/AddRol/`,params, {headers: headers}).map(res => res.json());
	}

	AsignarProceso(user: User): Observable<any>{
		let json = JSON.stringify(user);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.post(`${this._configApi.URL}usuario/AsignarProceso/`,params, {headers: headers}).map(res => res.json());
	}

	UpdateUser(user: User, idUsuario: any): Observable<any>{
		console.log('userservicio',user);
		let json = JSON.stringify(user);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}usuario/UpdateUsuario/${idUsuario}`, 
			params, {headers: headers}).map(res => res.json());  
	}

	ChangePass(user: User, idUsuario: number): Observable<any>{
		let json = JSON.stringify(user);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}usuario/ChangePass/${idUsuario}`, 
			params, {headers: headers}).map(res => res.json());  
	}

	ChangeFoto(user: User, idUsuario: number): Observable<any>{
		let json = JSON.stringify(user);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._Http.put(`${this._configApi.URL}usuario/ChangeFoto/${idUsuario}`, 
			params, {headers: headers}).map(res => res.json());  
	}

	DeleteUser(idUsuario: number): Observable<any>{
		return this._Http.delete(`${this._configApi.URL}usuario/DeleteUsuario/${idUsuario}`).map(res => res.json());
	}

/*=====  End of nuevos  ======*/
}