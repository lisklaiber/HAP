import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'menu-desktop',
  templateUrl: '../views/menu.html',
  styleUrls: ['../../assets/css/home.css']
})

export class MenuComponent implements OnInit{

	ngOnInit(){
		$(document).ready(function(){

    $('.prueba').click(function(){
		var getName = $(this).attr('name').split('.');
		var number = getName[1];
		// console.log(number);
		switch(number){
			case '1':
				$('#p1').html('&nbsp;Proceso&nbsp;');
				$('#img1').attr('src','../assets/img/Procesos azul.png');
				$(".agregar").css("display", "inline-block");
			break;

			case '2':
				$('#p1').html('&nbsp;Detalle&nbsp;');
				$('#img1').attr('src','../assets/img/detalle azul.png');
				$(".agregar").css("display", "inline-block");
			break;

			case '3':
				$('#p1').html('&nbsp;Indicadores&nbsp;');
				$('#img1').attr('src','../assets/img/indicadores azul.png');
				$(".agregar").css("display", "none");
			break;

			case '4':
				$('#p1').html('&nbsp;Crear Proceso&nbsp;');
				$('#img1').attr('src','../assets/img/crear proceso azul.png');
				$(".agregar").css("display", "none");
			break;

			case '5':
				$('#p1').html('&nbsp;Editar Proceso&nbsp;');
				$('#img1').attr('src','../assets/img/editar proceso azul.png');
				$(".agregar").css("display", "none");
			break;

			case '6':
				$('#p1').html('&nbsp;Agregar Plantilla&nbsp;');
				$('#img1').attr('src','../assets/img/agregar plantilla azul.png');
				$(".agregar").css("display", "none");
			break;

			case '7':
				$('#p1').html('&nbsp;Editar Plantilla&nbsp;');
				$('#img1').attr('src','../assets/img/editar plantilla azul.png');
				$(".agregar").css("display", "none");
			break;

			case '8':
				$('#p1').html('&nbsp;Rol&nbsp;');
				$('#img1').attr('src','../assets/img/rol azul.png');
				$(".agregar").css("display", "none");
			break;

			case '9':
				$('#p1').html('&nbsp;Agregar Usuario&nbsp;');
				$('#img1').attr('src','../assets/img/Agregar Rol azul.png');
				$(".agregar").css("display", "none");
			break;

			case '10':
				$('#p1').html('&nbsp;Editar Usuario&nbsp;');
				$('#img1').attr('src','../assets/img/editar rol azul.png');
				$(".agregar").css("display", "none");
			break;
		}
	});

});

	}

}