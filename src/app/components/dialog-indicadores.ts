import { Component, Inject, OnInit, HostListener, Input, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { IndicadorComp, IndicadorTC } from '../models/indicador';
import { Estrategia } from '../models/estrategia';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { IndicadorService } from '../services/indicadores.service';
import { EstrategiaService } from '../services/estrategia.service';
import { reject } from 'q';
import { DialogAlert } from './dialog-alert';
import { SnakBarInfoComponent } from './snak-bar-info.component';

@Component({
	selector: 'dialog-indicadores',
	styleUrls: ['../../assets/css/estructura-indicadores.css'],
	templateUrl: '../views/dialog-indicadores.html',
	providers:[IndicadorService, EstrategiaService]
})

export class DialogIndicadoresComponent {

	public arrayIndicadores: any [3];
	public index: number;
	public idxEstrategia: number;
	public titleIndicador: string;
	public limiteInf: string;
	public limiteSup: string;
	public errorMessage: any;

  	constructor(
        public dialogRef: MatDialogRef<DialogIndicadoresComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public _IndicadorService: IndicadorService,
		public _EstrategiaService: EstrategiaService,
		public dialog: MatDialog,
		public snakBArInfo: MatSnackBar
	){ 
		this.titleIndicador = "% Completado Verde"
		this.arrayIndicadores = [];
		this.limiteInf = "verdeMin";
		this.limiteSup = "verdeMax";
		this.index = 0;
		this.idxEstrategia = 0;
		
		this.arrayIndicadores[0] = {
			idIndicador: 0,
			indicador: new IndicadorComp('% Completado', 0.0, 0.0, 0.0, 0.0, 0.0, '', 0),
			estrategias: [
				{
					estrategia: new Estrategia('', '', 0, 0),
					idEstrategia: 0
				},
				{
					estrategia: new Estrategia('', '', 0, 0),
					idEstrategia: 0
				},
				{
					estrategia: new Estrategia('', '', 0, 0),
					idEstrategia: 0
				}
			],
			llenado1: false,
			llenado2: false,
			llenado3: false
		}

		for (let index = 1; index < 3; index++) {
			let nombre = "";
			let tipo = 0;
			if( index === 1 ){
				nombre = "Tiempo";
				tipo = 1;
			}
			else{
				nombre = "Costo";
				tipo = 2;
			}
			this.arrayIndicadores[index] = {
				idIndicador: 0,
				indicador: new IndicadorTC(nombre, tipo, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, '', 0),
				estrategias: [
					{
						estrategia: new Estrategia('', '', 0, 0),
						idEstrategia: 0
					},
					{
						estrategia: new Estrategia('', '', 0, 0),
						idEstrategia: 0
					},
					{
						estrategia: new Estrategia('', '', 0, 0),
						idEstrategia: 0
					},
					{
						estrategia: new Estrategia('', '', 0, 0),
						idEstrategia: 0
					},
					{
						estrategia: new Estrategia('', '', 0, 0),
						idEstrategia: 0
					}
				],
				llenado1: false,
				llenado2: false,
				llenado3: false,
				llenado4: false,
				llenado5: false
			}
		}

		// console.log(this.arrayIndicadores);
	}
	
	ngOnInit(): void {
		this.GetIndicadores();
		// this.openSnackBar("Ocurrión un problema al actualizar el Indicador, intentalo de nuevo");
	}


	onNoClick(): void {
    	this.dialogRef.close({indicadorChanged: true});
	}

	btnAplicarClick(): void{
		this.arrayIndicadores[this.index][`llenado${ this.idxEstrategia + 1 }`] = true;
		this.UpdateIndicador(this.index, this.idxEstrategia);
		if( this.index == 0 ){
			if( this.idxEstrategia < 2 ){
				this.idxEstrategia++;
			}
			else{
				this.index++;
				this.idxEstrategia = 0;
			}
		}
		else{
			if( this.index == 2 && this.idxEstrategia == 4 ){
				this.index = 0;
				this.idxEstrategia = 0;
			}
			else{
				if( this.idxEstrategia < 4 ){
					this.idxEstrategia++;
				}
				else{
					this.idxEstrategia = 0;
					this.index ++;
				}
			}
		}
		this.CambiarNombreLILS();
	}

	CambiarNombreLILS(avanza: boolean = false){
		if( this.index == 0 ){
			switch (this.idxEstrategia){
				case 0:{
					this.limiteInf = "verdeMin";
					this.limiteSup = "verdeMax";
					break;
				}
				case 1:{
					this.limiteInf = "amMin";
					this.limiteSup = "amMax";
					break;
				}
				case 2:{
					this.limiteInf = "rojo";
					break;
				}
			}
		}
		else{
			switch (this.idxEstrategia){
				case 0:{
					this.limiteInf = "verdeMin";
					this.limiteSup = "verdeMax";
					break;
				}
				case 1:{
					this.limiteInf = "amSupMin";
					this.limiteSup = "amSupMax";
					break;
				}
				case 2:{
					this.limiteInf = "amInfMin";
					this.limiteSup = "amInfMax";
					break;
				}
				case 3:{
					this.limiteInf = "rojoSup";
					break;
				}
				case 4:{
					this.limiteInf = "rojoInf";
					break;
				}
			}
		}
	}

	openSnackBar(texto: string) {
		this.snakBArInfo.openFromComponent(SnakBarInfoComponent, {
		  duration: 1000,
		  data: texto
		});
	  }

	UpdateIndicador(idxIndicador: number, idxEstrategia: number){
		let tipo = 1;
		if( idxIndicador > 0 )
			tipo = 2;

		this._IndicadorService.UpdateIndicador(this.arrayIndicadores[idxIndicador].idIndicador, this.arrayIndicadores[idxIndicador].indicador, tipo).subscribe(
			response => {
				if( response ){
					this.UpdateEstrategia(this.arrayIndicadores[idxIndicador].estrategias[idxEstrategia].idEstrategia, this.arrayIndicadores[idxIndicador].estrategias[idxEstrategia].estrategia, tipo);
				}
			}, error => {
				this.errorMessage = <any>error;
				if( this.errorMessage != null ){
					console.log(this.errorMessage);
					// this.dialogRef.close({ indicadorChanged: false });
					this.openSnackBar("Ocurrión un problema!");
					// this.openDialog("Ocurrión un problema al actualizar el Indicador, intentalo de nuevo");
				}
			}
		);
	}

	UpdateEstrategia(idEstrategia: number, estrategia:any, tipo: number){
		this._EstrategiaService.UpdateEstrategia(idEstrategia, estrategia, tipo).subscribe(
			response => {
				if(response)
					this.openSnackBar("Indicador actualizado!");
			},
			error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					// this.dialogRef.close({ indicadorChanged: false });
					this.openSnackBar("Ocurrión un problema!");
				}
			}
		);
	}

	IndicadorClick(index: number, idxEstrategia: number): void{
		this.index = index;
		this.idxEstrategia = idxEstrategia;
		this.CambiarNombreLILS();
	}

	IndicadorOpaco(index: number, idxEstrategia: number): boolean{
		if( this.arrayIndicadores[index][`llenado${ idxEstrategia + 1 }`] === true )
			return false;
		else if(this.index === index && this.idxEstrategia === idxEstrategia ){
			return false;
		}
		else
			return true;
	}

	IndicadorIsRojo(): boolean{
		if( this.index == 0 && this.idxEstrategia == 2 )
			return true;
		else {
			if( this.idxEstrategia == 4 )
				return true;
			else
				return false;
		}
	}

	numberOnly(event): boolean {
		const charCode = (event.which) ? event.which : event.keyCode;
		// console.log(charCode);
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			if(charCode == 46)
				return true
			else
				return false;
		}
		return true;
	}

	GetIndicadores(): void{
		this._IndicadorService.GetIndicadores(this.data.proceso).subscribe(
			resolve => {
				if( resolve.indicadorComp == undefined )
				{
					this.dialogRef.close({ indicadorChanged: false });
					this.openDialog("Ocurrión un problema al cargar los indicadores, intentalo de nuevo");
				}
				else{
					this.arrayIndicadores[0].idIndicador = resolve.indicadorComp.ID_Indicador;
					this.arrayIndicadores[0].indicador.nombre = resolve.indicadorComp.Nombre;
					this.arrayIndicadores[0].indicador.verdeMax = resolve.indicadorComp.VERDE_MAX;
					this.arrayIndicadores[0].indicador.verdeMin = resolve.indicadorComp.VERDE_MIN;
					this.arrayIndicadores[0].indicador.amMax = resolve.indicadorComp.AM_MAX;
					this.arrayIndicadores[0].indicador.amMin = resolve.indicadorComp.AM_MIN;
					this.arrayIndicadores[0].indicador.rojo = resolve.indicadorComp.ROJO;
					this.arrayIndicadores[0].indicador.descripcion = resolve.indicadorComp.Descripcion;
					this.arrayIndicadores[0].indicador.idProceso = resolve.indicadorComp.ID_Proceso;
	
					this.arrayIndicadores[1].idIndicador = resolve.indicadorT.ID_Indicador;
					this.arrayIndicadores[1].indicador.nombre = resolve.indicadorT.Nombre;
					this.arrayIndicadores[1].indicador.tipo = resolve.indicadorT.Tipo;
					this.arrayIndicadores[1].indicador.verdeMax = resolve.indicadorT.VERDE_MAX;
					this.arrayIndicadores[1].indicador.verdeMin = resolve.indicadorT.VERDE_MIN;
					this.arrayIndicadores[1].indicador.amSupMax = resolve.indicadorT.AM_SUP_MAX;
					this.arrayIndicadores[1].indicador.amSupMin = resolve.indicadorT.AM_SUP_MIN;
					this.arrayIndicadores[1].indicador.amInfMax = resolve.indicadorT.AM_INF_MAX;
					this.arrayIndicadores[1].indicador.amInfMin = resolve.indicadorT.AM_INF_MIN;
					this.arrayIndicadores[1].indicador.rojoSup = resolve.indicadorT.ROJO_SUP;
					this.arrayIndicadores[1].indicador.rojoInf = resolve.indicadorT.ROJO_INF;
					this.arrayIndicadores[1].indicador.descripcion = resolve.indicadorT.Descripcion;
					this.arrayIndicadores[1].indicador.idProceso = resolve.indicadorT.ID_Proceso;
	
					this.arrayIndicadores[2].idIndicador = resolve.indicadorC.ID_Indicador;
					this.arrayIndicadores[2].indicador.nombre = resolve.indicadorC.Nombre;
					this.arrayIndicadores[2].indicador.tipo = resolve.indicadorC.Tipo;
					this.arrayIndicadores[2].indicador.verdeMax = resolve.indicadorC.VERDE_MAX;
					this.arrayIndicadores[2].indicador.verdeMin = resolve.indicadorC.VERDE_MIN;
					this.arrayIndicadores[2].indicador.amSupMax = resolve.indicadorC.AM_SUP_MAX;
					this.arrayIndicadores[2].indicador.amSupMin = resolve.indicadorC.AM_SUP_MIN;
					this.arrayIndicadores[2].indicador.amInfMax = resolve.indicadorC.AM_INF_MAX;
					this.arrayIndicadores[2].indicador.amInfMin = resolve.indicadorC.AM_INF_MIN;
					this.arrayIndicadores[2].indicador.rojoSup = resolve.indicadorC.ROJO_SUP;
					this.arrayIndicadores[2].indicador.rojoInf = resolve.indicadorC.ROJO_INF;
					this.arrayIndicadores[2].indicador.descripcion = resolve.indicadorC.Descripcion;
					this.arrayIndicadores[2].indicador.idProceso = resolve.indicadorC.ID_Proceso;
	
					this.GetEstrategias(this.arrayIndicadores[0].idIndicador, 1);
					this.GetEstrategias(this.arrayIndicadores[1].idIndicador, 2);
					this.GetEstrategias(this.arrayIndicadores[2].idIndicador, 3);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.dialogRef.close({ indicadorChanged: false });
					this.openDialog("Ocurrión un problema al cargar los indicadores, intentalo de nuevo");
				}
			}
		);
	}

	GetEstrategias(idIndicador: number, tipo: number): void{
		let tipoIndicador:  number = 1;
		if( tipo > 1 )
			tipoIndicador = 2;
		this._EstrategiaService.GetEstrategias(idIndicador, tipoIndicador).subscribe(
			resolve => {
				for (let index = 0; index < resolve.resp.length; index++) {
					this.arrayIndicadores[tipo - 1].estrategias[index].idEstrategia = resolve.resp[index].ID_Estrategia;
					this.arrayIndicadores[tipo - 1].estrategias[index].estrategia.nombre = resolve.resp[index].Nombre;
					this.arrayIndicadores[tipo - 1].estrategias[index].estrategia.descripcion = resolve.resp[index].Descripcion;
					this.arrayIndicadores[tipo - 1].estrategias[index].estrategia.idIndicador = resolve.resp[index].ID_Indicador;
					this.arrayIndicadores[tipo - 1].estrategias[index].estrategia.semaforo = resolve.resp[index].Semaforo;
					if(this.arrayIndicadores[tipo - 1].estrategias[index].estrategia.nombre == '')
						this.arrayIndicadores[tipo - 1][`llenado${ index + 1 }`] = false;
					else
						this.arrayIndicadores[tipo - 1][`llenado${ index + 1 }`] = true;
				}

				// console.log('array indicadores', this.arrayIndicadores);
				
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.dialogRef.close({ indicadorChanged: false });
				}
			}
		);
	}

	openDialog(texto: string){
		let dialogRef = this.dialog.open(DialogAlert, {
			width: '350px',
			data: texto
		});
	}
}
