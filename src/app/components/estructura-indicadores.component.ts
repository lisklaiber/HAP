import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProcesoService } from '../services/proceso.service';
import { MatDialog } from '@angular/material';
import { DialogIndicadoresComponent } from './dialog-indicadores';


//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'struct-indic',
	templateUrl: '../views/estructura-indicadores.html',
	styleUrls: ['../../assets/css/estructura-indicadores.css'],
	providers: []
})

export class StructIndicComponent implements OnInit{
	public procesos: any = [];
	public idProcesoPadre: number;
	public i: number;
	public levels: any = [];
	public title: string;
	public rutas: any[];
	public errorMessage: any;
	@Input() procesoBase: any;
	@Output() avanzarEvent = new EventEmitter<any>()

	constructor(
		private _ProcesoService: ProcesoService,
		public dialog: MatDialog
	){
		this.title = "Selecciona un elemento para editar";
		this.rutas = [];
	}
	
	ChangeRuta(idx: number){
		this.idProcesoPadre = this.rutas[idx].idProceso;
		this.GetProcesosHijos(this.rutas[idx].idProceso);
		this.rutas.splice(idx + 1, this.rutas.length - idx);
	}

	ngOnInit(){
		// this.procesoBase = {objeto: {nombre: "Atsan", subID: 0, tipo: 1, idProcBase: 0}, idProceso: 1, tipo: 1};
		this.idProcesoPadre = this.procesoBase.idProceso;
		this.rutas.push({ texto: `${this.procesoBase.objeto.nombre}`, idProceso: this.procesoBase.idProceso, idx: null});
		this.GetProcesosHijos(this.procesoBase.idProceso);
	}

	Avanzar(){
		this.avanzarEvent.emit();
	}

	BtnAccederClick(obj: any){
		// console.log(obj);
		this.rutas.push({ texto: `${obj.objeto.Nombre}`, idProceso: obj.objeto.ID_Proceso});
		this.idProcesoPadre = obj.objeto.ID_Proceso;
		this.GetProcesosHijos(obj.objeto.ID_Proceso);
	}

	BtnAsignarIndicadoresClick(idProceso: number, nombre: string){
		let dialogRef = this.dialog.open(DialogIndicadoresComponent, {
			disableClose: true,
			data: { title: nombre, proceso: idProceso  }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				if(result.procesoChanged){
                    // this.UpdateProceso();
                    console.log('elemente changed', result);
				}
			}
		});
	}

	BtnRegresar(){
		let idx = this.rutas.length - 2;
		this.idProcesoPadre = this.rutas[idx].idProceso;
		this.GetProcesosHijos(this.rutas[idx].idProceso);
		this.rutas.splice(idx + 1, this.rutas.length - idx);
	}

	GetProcesosHijos(idProceso: number = 1){
		this._ProcesoService.GetHijos(idProceso).subscribe(
			result => {
				this.procesos = [];
				for(let level of result.resp ){
					this.GetHijos(level.Hijo, level.Tipo_Hijo);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	
	
	GetHijos(hijo: number, tipo: number){
			if(tipo == 1){
					this.GetProceso(hijo);
			} else if (tipo == 2){
					this.GetEvento(hijo);
			} else{
					this.GetDecision(hijo);
			}
	}
	
	GetProceso(idProceso){
		this._ProcesoService.GetProceso(idProceso).subscribe(
			result => {
				this.procesos.push({objeto: result.resp, tipo: 1, idProcesoPadre: this.idProcesoPadre});
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	
	GetEvento(idEvento){
		this._ProcesoService.GetEvento(idEvento).subscribe(
			result => {
				this.procesos.push({objeto: result.resp, tipo: 2, idProcesoPadre: this.idProcesoPadre});
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	
	GetDecision(idDecision){
		this._ProcesoService.GetDecision(idDecision).subscribe(
			result => {
				this.procesos.push({objeto: result.resp, tipo: 3, idProcesoPadre: this.idProcesoPadre});
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	
}