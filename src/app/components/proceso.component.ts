import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ProcesoService } from '../services/proceso.service';
import { Proceso } from '../models/proceso';
import { MatDialog } from '@angular/material';
import { DialogPeocesoComponent } from './dialog-proceso.component';
import { DialogRamasComponent } from './dialog-ramas.component';
import { DialogAlert } from './dialog-alert';
import { Level } from '../models/level';
import { Serial } from '../models/serial';
import { DialogHijosComponent } from './dialog-hijos.component';


@Component({
	selector: 'proceso',
	templateUrl: '../views/proceso.html',
	styleUrls: ['../../assets/css/add-plantilla.css', 
    '../../assets/css/flujo.css', 
    '../../assets/css/add-user.css', 
    '../../assets/css/edit-user.css',
    '../../assets/css/estructura-proceso.css'],
	providers: [
		ProcesoService
	]
})

export class ProcesoComponent implements OnInit{

    @Input() element: any;
    @Input() idx: number;
    @Input() subIdProcesoPadre: number;
    @Input() idBase: number;
    @Input() idPadre: number;
    @Input() intoActividad: boolean;
    @Input() ramaPadre: number;
    @Output() DeleteEvent = new EventEmitter<number>();
    @Output() AccederProc = new EventEmitter<number>();
    @Output() PushLevel = new EventEmitter<any>();
    @Output() PushSerial = new EventEmitter<any>();
    public errorMessage: any;
    public hijos: any[];
    public levelAsign: boolean;
    
    constructor(
        public dialog: MatDialog,
        public _ProcesoService: ProcesoService
    ){
        this.element = {};
        this.idx = 0;
        this.hijos = [];
        this.levelAsign = false;
    }

    ngOnInit(){
        // console.log(this.element);
    }

    focusFunction(element){
		element.setSelectionRange(0, element.size);
    }
    
    DeleteItem(){
        this.DeleteEvent.emit(this.idx);
    }

    BtnEditarClick(){
        let dialogRef = this.dialog.open(DialogPeocesoComponent, {
			
			data: { title: "Editar Proceso", proceso: this.element  }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				if(result.procesoChanged){
                    this.UpdateProceso();
                    // console.log('elemente changed', this.element);
				}
			}
		});
    }

    UpdateProceso(){
        // console.log('element', this.element);
        this._ProcesoService.UpdateProceso(this.element.idProceso, this.element.objeto, this.element.tipo).subscribe(
            response => {
                // console.log(response.resp);
                if(!response.resp){
                    this.OpenDialog("Parece que no se realizaron los cambios");
                }
                else{
                    if(this.levelAsign == false){
                        this.AddLevel(this.element.idProceso, this.element.tipo);
                        this.levelAsign = true;
                    }
                }
            },
            reject => {
                this.errorMessage = <any>reject;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.OpenDialog("Ups!, Ocurrión un problema y no se guardaron los cambios");
				}
            }
        );
    }

    AddLevel(hijo: number, tipoHijo: number){
        let level = new Level(this.idBase, this.idPadre, hijo, tipoHijo);
        // console.log('level',level);
		this._ProcesoService.AddLevel(level).subscribe(
			response =>{
                // this.levels.push({ level: level, idLevel: response.resp[0] });
                this.PushLevel.emit({ level: level, idLevel: response.resp[0] });
			},
			reject =>{
				this.errorMessage = <any>reject;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.OpenDialog("Ocurrión un problema");
				}
			}
		);
    }

    btnSerializarClick(){
        this.OpenHijos();
    }

    OpenHijos(): void {
		let dialogRef = this.dialog.open(DialogHijosComponent, {
			data: { title: "Selecciona los elementos que precedan", idPadre: this.idPadre, idProceso: this.element.idProceso }
		});
		dialogRef.afterClosed().subscribe(result => {
            // console.log('ramaChanged', this.element);
            if(result.serialChanged && result.hijos !== undefined)
                this.AddSerials(result.hijos);
                // console.log('procesos selected', result.hijos);
                // this.AccederProc.emit(this.idx);
		});	
    }
    
    AddSerials(hijos: any){
        let serial;
        for(let hijo of hijos){
            if(hijo.ID_Proceso !== undefined){
                serial = new Serial(this.idBase, hijo.ID_Proceso, this.element.idProceso, 1, this.element.tipo);
            }else if(hijo.ID_Evento !== undefined){
                serial = new Serial(this.idBase, hijo.ID_Evento, this.element.idProceso, 2, this.element.tipo);
            } else{
                serial = new Serial(this.idBase, hijo.ID_Desicion, this.element.idProceso, 3, this.element.tipo);
            }
            this._ProcesoService.AddSerial(serial).subscribe(
                response => {
                    if(response.resp)
                        this.PushSerial.emit({serial: serial, id: response.resp[0]});
                },
                reject =>{
                    this.errorMessage = <any>reject;
                    if(this.errorMessage != null){
                        console.log(this.errorMessage);
                        this.OpenDialog("Ocurrión un problema");
                    }
                }
            );
        }
        
         
    }

    OpenDialog(texto: string): void {
		let dialogRef = this.dialog.open(DialogAlert, {
            width: '350px',
            data: texto
		});
	}

    dialogRamas(): void {
		let dialogRef = this.dialog.open(DialogRamasComponent, {
			data: { title: "Selecciona el número de ramas", proceso: this.element  }
		});
		dialogRef.afterClosed().subscribe(result => {
            // console.log('result', result);
            if(result){
                if(result.procesoChanged)
                    this.AccederProc.emit(this.idx);
            }
		});	
    }
    
    btnAccederClick(){
        if(this.element.ramas < 1){
            this.dialogRamas();
        }
        else{
            this.AccederProc.emit(this.idx);
        }
    }
    
    onChange(value: number, tipo: number){
        // console.log(value);
        if(tipo == 3){
            if(this.element.objeto.tipo == 3){
                this.element.ramas = 0;
            }
        }
        this.UpdateProceso();
    }
    
    /* funcion que nos ayuda a navegar en el caminio */
}