import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

/* Models */
import { Proceso } from '../models/proceso';
import { Evento } from '../models/evento';
import { Desicion } from '../models/desicion';

/* Services */
import { ProcesoService } from '../services/proceso.service';

/* Components */
import { DialogAlert } from './dialog-alert';
import { MatDialog } from '@angular/material';

@Component({
	selector: 'struct-proc',
	templateUrl: '../views/estructura-proceso.html',
	styleUrls: ['../../assets/css/add-plantilla.css', 
		'../../assets/css/estructura-proceso.css'],
	providers: [ProcesoService]
})

export class StructProcComponent implements OnInit{
	@Input() procesoBase: any;

	public procesos: any[] = [];
	public i: number;
	public levels: any[] = [];
	public serials: any[] = [];
	public subIdProcesoPadre: number;
	public btnSubNivel: boolean;
	public rutas: any[];
	public procesoPadre: any;
	public txtBtnEdit: string;
	public errorMessage: any;
	public ramasPadre: number;
	public ramaPadre: number;
	public ramas: any [];

	constructor(
		private _ProcesoService: ProcesoService,
		private dialog: MatDialog
	){
		this. i = 1;
		let nombreProc = "Proceso Gral";
		let abreviatura = 'PG';
		this.subIdProcesoPadre = 0;
		this.btnSubNivel = false; 
		this.procesoPadre = {objeto: new Proceso(0, nombreProc, abreviatura, new Date(), new Date(), 0, true, 1, 0, 'Descripcion del Proceso General', 0, 1, 0),
			id: null,
			tipo: 1};

		// this.procesos.push(new Proceso(this.i, nombreProc, abreviatura,  new Date(), new Date(), 0, true, 1, 0, 'Descripcion...', 0, 1, 0));
		// this.levels.push({Padre: this.subIdProcesoPadre, Hijo: this.i});
		this.rutas = [{ texto: "Base /", idProceso: this.subIdProcesoPadre, idx: null}];
		this.ChangeNameBtnEdit();
		this.ramasPadre = 1;
		this.ramas = [];
		this.ramaPadre = 1;
	}

	ChangeNameBtnEdit(){
		// console.log('proceso padre condicion', !this.procesoPadre.condicion);
		if(this.procesoPadre.tipo == 1){
			if(this.procesoPadre.objeto.tipo == 2)
				this.txtBtnEdit = "+ Paso";
			else
				this.txtBtnEdit = "+ Proceso o Actividad";
		}
		else
			this.txtBtnEdit = "+ Proceso o Actividad";
	}

	IntoActividad(): boolean{
		if(this.procesoPadre.tipo == 1){
			if(this.procesoPadre.objeto.tipo == 2)
				return true;
			else
				return false;
		}
		else
			return false;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
		this.procesoPadre = this.procesoBase;
		// console.log(this.procesoPadre);
	}
	
	AddObj ( obj: number ): void{
		let tipo: number = 0;
		let nombreProc: string;
		let idProcesoPredecesor = this.GetPredecesor();
		if(obj === 1){
			if(this.procesoPadre.tipo > 1){
				tipo = 1;
				nombreProc = "Proceso";
			}
			else{
				if(this.procesoPadre.objeto.tipo == 1){
					tipo = 1;
					nombreProc = "Proceso";
				}
				else{
					tipo = 3;
					nombreProc = "Paso";
				}
			}
			
			// if(idProcesoPredecesor > 0)
				// this.serials.push( new Serial(this.procesoBase.ID_Proceso, idProcesoPredecesor, ID_Seriado: this.i));

			// this.levels.push({Padre: this.subIdProcesoPadre, Hijo: this.i});
			this.procesos.push({ 
				objeto: new Proceso(this.i, nombreProc, this.GetAbreviatura(nombreProc),  new Date(), new Date(), 0, true, tipo, 0, 
				'Descripcion...', 0, 1, this.procesoBase.idProceso), 
				tipo: obj,
				tipoPadre: this.procesoPadre.tipo,
				ramas: 0,
				ramaPadre: this.ramaPadre,
				idProceso: null,
				subIdProcesoPadre: this.subIdProcesoPadre
			});
		}
		else if (obj == 2){
			this.procesos.push({
				objeto: new Evento('Evento', 'Descripcion...'),
				tipo: obj,
				tipoPadre: this.procesoPadre.tipo,
				ramas: 0,
				ramaPadre: this.ramaPadre,
				idProceso: null,
				subIdProcesoPadre: this.subIdProcesoPadre,
				subId: this.i
			});
		}
		else{
			this.procesos.push({
				objeto: new Desicion(1, 'Desición', 'Descripcion...', 'Condición lógica...'),
				tipo: obj,
				tipoPadre: this.procesoPadre.tipo,
				ramas: 2,
				ramaPadre: this.ramaPadre,
				idProceso: null,
				subIdProcesoPadre: this.subIdProcesoPadre,
				subId: this.i
			});
			
		}
		this.SubirProceso();
		this.AsignarRamas();
		this.i++;

		// console.log('objeto', this.procesos[this.procesos.length - 1]);
		// console.log('obj', obj);
		// console.log("Procesos: ", this.procesos);
		// console.log("levels: ", this.levels);
		// console.log("Serials: ", this.serials);
	}

	AsignarRamas(){
		let procesosSameLevel = this.GetProcesosSameLevel();
		let idx = procesosSameLevel.length - 2;
		let idxprocesos: number;

		if(procesosSameLevel[idx] !== undefined ){
			if(procesosSameLevel[idx].subID === undefined)
				idxprocesos = procesosSameLevel[idx].objeto.subID;
			else
				idxprocesos = procesosSameLevel[idx].subId;
	
			if(procesosSameLevel[idx].ramas == 0 ){
				procesosSameLevel[idx].ramas = 1;
			}
		
		}
	}

	SubirProceso(){
		let idx = this.procesos.length - 1;
		this._ProcesoService.AddProceso(this.procesos[idx].objeto, this.procesos[idx].tipo).subscribe(
			response =>{
				if(this.procesos[idx].tipo == 1)
					this.procesos[idx].idProceso = response.resp[0];
				else if (this.procesos[idx].tipo == 2)
					this.procesos[idx].idProceso = response.idEvento[0];
				else
					this.procesos[idx].idProceso = response.idDecision[0];
				// console.log('Procesos', this.procesos);
			}, 
			reject =>{
				this.errorMessage = <any>reject;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.dialogErrorAddProc("Ocurrión un problema", idx);
				}
			}
		);
	}

	PushLevel(level: any){
		this.levels.push(level);
	}

	PushSerial(serial: any){
		this.serials.push(serial);
	}

	dialogErrorAddProc(texto: string, idx: number): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto//{ name: this.name, animal: this.animal }
		});
		dialogRef.afterClosed().subscribe(result => {
			this.procesos.splice(idx, 1);
		});	
	}

	GetAbreviatura(nombre):string{
		let palabras = nombre.split(' ');
		let abreviatura:string = nombre[0];

		for(let i = 0; i < palabras.length; i++){
			if (i >= 1)
				break;
			abreviatura += palabras[i][0];
		}
		return abreviatura;
	}

	isFirst(index: number, idBase: number): boolean{
		let procesosSameLevel: Proceso[] = this.GetProcesosSameLevel();

		if(!this.procesos[index - 1] )
			return true;
		else{
			if(procesosSameLevel[0].subID == this.procesos[index].subID)
				return true;
			else 
				return false;
		}
	}

	ObjFinalRamas(): boolean{
		let procesos = this.GetProcesosSameLevel();
		if(procesos.length > 0) {
			let idx = procesos.length - 1;
			if(procesos[idx].tipo != 1)
				return true;
			else{
				if(procesos[idx].ramas > 1){
					return true;
				}
				else
					return false;
			}
		}
		else
			return false;
	}

	GetProcesosSameLevel(): any[]{
		let procesosSameLevel: any[] = []

		this.procesos.map((proceso, index) =>{
			if(proceso.subIdProcesoPadre === this.subIdProcesoPadre && this.ramaPadre === proceso.ramaPadre){
				procesosSameLevel.push(proceso);
			}
		});

		return procesosSameLevel;
	}

	GetPredecesor(): number{
		
		let procesosSameLevel: number[] = [];

		this.procesos.map((proceso, index) =>{
			if(proceso.idProcBase === this.subIdProcesoPadre){
				procesosSameLevel.push(proceso.subID);
			}
		});
		// console.log("procesosSameLevel: ", procesosSameLevel)
		if(procesosSameLevel.length > 0)
			return Math.max( ...procesosSameLevel );
		else
			return 0;
	}
	

	AccederProceso(idx: number){
		this.ramasPadre = this.procesos[idx].ramas;
		if(this.procesos[idx].tipo > 1){
			this.subIdProcesoPadre = this.procesos[idx].subId;
			this.rutas.push({texto: ` ${ this.procesos[idx].objeto.nombre } /`, idProceso: this.procesos[idx].subId, idx: idx});
			this.CreateRamas(this.procesos[idx].subId, idx);
		}
		else{
			this.subIdProcesoPadre = this.procesos[idx].objeto.subID;
			if(this.procesos[idx].ramas == 1)
				this.procesoPadre = this.procesos[idx];
			this.rutas.push({texto: ` ${ this.procesos[idx].objeto.nombre } /`, idProceso: this.procesos[idx].objeto.subID, idx: idx});
			this.CreateRamas(this.procesos[idx].objeto.subID, idx);
		}	

		this.ToogleBtnSubNivel();
		this.ChangeNameBtnEdit();
	}

	CreateRamas(subIdProcesoPadre: number, idx: number){
		if(this.ramasPadre > 1){
			this.ramas = [];
			for (let i = 0; i < this.ramasPadre; i++) {
				this.ramas.push({subIdProcesoPadre: subIdProcesoPadre, noRama: i, idx: idx});
			}
			this.ramaPadre = null;
		}else{
			this.ramaPadre = 1;
		}
	}

	AccederRama(rama: any){
		this.rutas.push({texto: ` Rama ${ rama.noRama + 1 } /`, idProceso: rama.subIdProcesoPadre, rama: rama.noRama, idx: rama.idx});
		this.ramasPadre = 1;
		this.ramaPadre = rama.noRama;
		this.ToogleBtnSubNivel();
		this.ChangeNameBtnEdit();
	}

	btnRegresarClick(){
		let idx = this.rutas.length - 2;
		this.subIdProcesoPadre = this.rutas[idx].idProceso;
		this.rutas.splice(idx + 1, this.rutas.length - idx);
		if(this.rutas[idx].idx === null || this.rutas[idx].idx === undefined){
			this.procesoPadre = this.procesoBase;
			this.ramasPadre = 1;
			this.ramaPadre = 1;
		}
		else{
			if(this.procesos[this.rutas[idx].idx].tipo == 1)
				this.procesoPadre = this.procesos[this.rutas[idx].idx];
			else{
				this.procesoPadre = this.procesos[this.procesos[this.rutas[idx].idx].subIdProcesoPadre];
			}
			if(this.rutas[idx].rama === undefined ){
				this.ramasPadre = this.procesos[this.rutas[idx].idx].ramas;
				this.ramaPadre = 1;
				if(this.procesos[this.rutas[idx].idx].tipo > 1)
					this.CreateRamas(this.procesos[this.rutas[idx].idx].subId, this.rutas[idx].idx);
				else
					this.CreateRamas(this.procesos[this.rutas[idx].idx].objeto.subID, this.rutas[idx].idx);
			}
			else{
				this.ramasPadre = 1;
				this.ramaPadre = this.rutas[idx].rama;
			}
		}
		this.ToogleBtnSubNivel();
		this.ChangeNameBtnEdit();
	}

	ChangeRuta(indexRuta: number){
		this.subIdProcesoPadre = this.rutas[indexRuta].idProceso;
		this.rutas.splice(indexRuta + 1, this.rutas.length - indexRuta);
		if(this.rutas[indexRuta].idx === null || this.rutas[indexRuta].idx === undefined){
			this.procesoPadre = this.procesoBase;
			this.ramasPadre = 1;
			this.ramaPadre = 1;
		}
		else{
			if(this.procesos[this.rutas[indexRuta].idx].tipo == 1)
				this.procesoPadre = this.procesos[this.rutas[indexRuta].idx];
			if(this.rutas[indexRuta].rama === undefined ){
				this.ramasPadre = this.procesos[this.rutas[indexRuta].idx].ramas;
				this.ramaPadre = null;
				if(this.procesos[this.rutas[indexRuta].idx].tipo > 1)
					this.CreateRamas(this.procesos[this.rutas[indexRuta].idx].subId, this.rutas[indexRuta].idx);
				else
					this.CreateRamas(this.procesos[this.rutas[indexRuta].idx].objeto.subID, this.rutas[indexRuta].idx);
			}
			else{
				this.ramasPadre = 1;
				this.ramaPadre = this.rutas[indexRuta].rama;
			}
		}
		this.ToogleBtnSubNivel();
		this.ChangeNameBtnEdit();
	}

	removeProc(index){
		// let idProceso = this.procesos[index].subID;
		let tipo = this.procesos[index].tipo;
		let idProceso = this.procesos[index].idProceso;

		this._ProcesoService.DeleteProceso(idProceso, tipo).subscribe(
			response => {
				if(response.resp)
					this.procesos.splice(index, 1);
					else
						this.dialogErrorDeleteProc("Ocurrión un problema");
			}, 
			reject => {
				this.errorMessage = <any>reject;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.dialogErrorDeleteProc("Ocurrión un problema");
				}
			}
		);
		
		// this.removeChilds(idProceso);
	}

	dialogErrorDeleteProc(texto: string): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto
		});
	}

	removeChilds(idProceso){
		let indices: number[] = [];

		for(let i = 0; i < this.procesos.length; i++){
			if(this.procesos[i].idProcBase == idProceso){
				this.procesos.splice(i, 1);
				i = 0;
			}
		}

		for(let i = 0; i < this.levels.length; i++){
			if(this.levels[i].padre == idProceso){
				this.removeChilds(this.levels[i].hijo);
				i = 0;
			}
			if(this.levels[i].hijo == idProceso){
				this.levels.splice(i, 1);
				i = 0;
			}
		}

	}

	onlyUnique(value, index, self) { 
    	return self.indexOf(value) === index;
	}

	focusFunction(element){
		element.setSelectionRange(0, element.size);
		// console.log(element);
	}

	ToogleBtnSubNivel(){
		if(this.subIdProcesoPadre == 0){
			this.btnSubNivel = false;
		}else{
			this.btnSubNivel = true;
		}
	}
}