import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSelectModule,} from '@angular/material';
import { Rol } from '../models/rol';



@Component({
  selector: 'dialog-Rol',
  templateUrl: '../views/dialog-Rol.html'
})
export class DialogRolComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  		if(!data.rol){
  			data.rol ={
          Descripcion:"Descripcion...",
          Nombre:"Nombre...",
          Color:"#ccc",
          Icono:"icono", 
          ID_Empresa: 1
        };
  		}
    }

  onNoClick(): void {
    this.dialogRef.close({rolChanged: false});
  }

}
