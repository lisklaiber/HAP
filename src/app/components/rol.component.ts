import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

/*PopUp's*/
import { DialogRolComponent } from '../components/dialog-Rol.component';
import { DialogAlert } from './dialog-alert';
import { DialogConfirmComponent } from './dialog-confirm.component';
import { DialogPermisosRolComponent} from '../components/dialog-permisos-rol.component'

/* Servicios */
import { RolService } from '../services/rol.service';

/* models */
import { Rol } from '../models/rol';

@Component({
	selector: 'rol',
	templateUrl: '../views/rol.html',
	styleUrls: ['../../assets/css/rol.css'],
	providers: [RolService]
})

export class RolComponent implements OnInit{
	private roles: any[];
	private idEmpresa: number;
	private errorMessage: any;


	constructor(
		private _RolService: RolService,
		private _route: ActivatedRoute,
		private _router: Router,
		public dialog: MatDialog
	){
		this.roles = [];
		this.idEmpresa = 1;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
		this.GetRoles();

	}

	openDialog(texto, isInit): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto
		});
		dialogRef.afterClosed().subscribe(result => {
			if(!isInit)
				this.GetRoles();
		});
	}

	btnEditarPermisos(idRol: number){
        this.OpenHijos(idRol);
    }

	 OpenHijos(idRol: number): void {
		let dialogRef = this.dialog.open(DialogPermisosRolComponent, {
			data: { title: "Selecciona los permisos", idRol: idRol  }
		});
		dialogRef.afterClosed().subscribe(result => {
            // console.log('ramaChanged', this.element);
            if(result.serialChanged && result.hijos !== undefined)
                // this.AddSerials(result.hijos);
                console.log('procesos selected', result.hijos);
                // this.AccederProc.emit(this.idx);
		});	
    }


	GetRoles(){
		this._RolService.GetRoles(this.idEmpresa).subscribe(
			response => {
					this.roles = response.resp;
					console.log(this.roles);
			},error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! No se pudieron cargar los roles", true);
					console.log(this.errorMessage);
					// alert("Usuario no encontrado");
				}
			}
		);
	}

	btnEditarClick(indexRol: number): void {
		let dialogRef = this.dialog.open(DialogRolComponent, {
			
			data: { title: "Editar Rol", rol: this.roles[indexRol]  }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				if(result.rolChanged){
					this.UpdateRol(result.rol);
				}
			}
		});
	}

	UpdateRol(rol: any){
		// console.log(rol);
		let rolmodel = new Rol(rol.Descripcion, rol.Nombre, rol.Color, rol.Icono, rol.ID_Empresa);
		this._RolService.UpdateRol(rolmodel,rol.ID_Rol).subscribe(
			response => {
				if(response.resp == 'correcto'){
					this.openDialog("Rol Actualizado Correctamente", false);
				}
				else{
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(response.error);
				}
			},
			error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(this.errorMessage);
					// alert("Usuario no encontrado");
				}
			}
		);
	}

	btnAddClick(){
		let dialogRef = this.dialog.open(DialogRolComponent, {
			width: '350px',
			data: { title: "Nuevo Rol"  }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				if(result.rolChanged){
					this.AddRol(new Rol(result.rol.Descripcion,result.rol.Nombre,result.rol.Color,result.rol.Icono,result.rol.ID_Empresa)); 
					// console.log('result.rol',result.rol)
					// console.log('rol',rol)
				}
			}
		});
	}

	AddRol(rol: Rol){
		this._RolService.AddRol(rol).subscribe(
			response => {
				this.openDialog("Rol agregado correctamente", false);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(this.errorMessage);
				}
			}
		);
	}

	btnEliminarClick(indexRol: number){
		let dialogRef = this.dialog.open(DialogConfirmComponent, {
			width: '300px',
			data: `¿Está seguro de eliminar el rol "${this.roles[indexRol].Nombre}"?`
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				this.DeleteRol(this.roles[indexRol].ID_Rol);
			}
		});
	}

	DeleteRol(idRol: number){
		this._RolService.DeleteRol(idRol).subscribe(
			response =>{
				if(response.resp == 1){
					this.openDialog("Rol eliminado correctamente", false);
				} else{
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(response.error);
				}
			},
			error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(this.errorMessage);
				}
			}
		);
	}


}