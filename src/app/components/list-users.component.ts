import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

/*PopUp's*/
import { DialogUserComponent } from '../components/dialog-User.component';
import { DialogAlert } from './dialog-alert';
import { DialogConfirmComponent } from './dialog-confirm.component';

/*Services*/
import { UserService } from '../services/user.service';

//import { Autentication } from '../models/autentication';
import {User} from '../models/user';

@Component({
	selector: 'list-user',
	templateUrl: '../views/list-users.html',
	styleUrls: ['../../assets/css/list-users.css'],
	providers: [UserService]
})

export class ListUsersComponent implements OnInit{

	private idEmpresa: number;
	private errorMessage: any;
	private users: any[];

	constructor(
		private _UserService: UserService,
		private _route: ActivatedRoute,
		private _router: Router,
		public dialog: MatDialog

	){
		this.idEmpresa = 1;	
		this.users = [];
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
		this.GetUsers();
	}

	openDialog(texto, isInit): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto
		});
		dialogRef.afterClosed().subscribe(result => {
			if(!isInit)
				this.GetUsers();
		});
	}

	GetUsers(){
		this._UserService.GetUsers(this.idEmpresa).subscribe(
			response => {
				this.users = response.resp;
				console.log(this.users);
			}, error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("No se pudieron cargar los usuarios!", true);
					console.log(this.errorMessage);
					// alert("Usuario no encontrado");
				}
			}
		);
	}

	btnEditarClick(indexUser: number): void {
		let dialogRef = this.dialog.open(DialogUserComponent, {
			
			data: { title: "Editar Usuario", user: this.users[indexUser]  }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				if(result.userChanged){
					this.UpdateUser(result.user);
				}
			}
		});
	}

	UpdateUser(user: any){
		// console.log(user);
		let usermodel = new User(user.ID_Empresa, user.Correo, user.Pass, user.Nombre, user.Ap_Paterno, user.Ap_Materno, user.Foto, user.Color);
		// console.log("usermodel",usermodel)
		this._UserService.UpdateUser(usermodel,user.ID_Usuario).subscribe(
			response => {
				if(response.resp == 'correcto'){
					this.openDialog("Usuario Actualizado Correctamente", false);
				}
				else{
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(response.error);
				}
			},
			error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(this.errorMessage);
					// alert("Usuario no encontrado");
				}
			}
		);
	}

	btnEliminarClick(indexUser: number){
		let dialogRef = this.dialog.open(DialogConfirmComponent, {
			width: '300px',
			data: `¿Está seguro de eliminar el usuario "${this.users[indexUser].Nombre}"?`
		});

		dialogRef.afterClosed().subscribe(result => {
			if(result){
				this.DeleteUser(this.users[indexUser].ID_Usuario);
			}
		});
	}

	DeleteUser(idUsuario: number){
		this._UserService.DeleteUser(idUsuario).subscribe(
			response =>{
				if(response.resp == 1){
					this.openDialog("Usuario eliminado correctamente", false);
				} else{
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(response);
				}
			},
			error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! Ocurrió un error", false);
					console.log(this.errorMessage);
				}
			}
		);
	}

	GetFoto(foto: string){
		return `174.138.0.228:5000/api/usuario/GetFoto/${foto}`;
	}


}
