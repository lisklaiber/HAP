import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'dialog-confirm',
  templateUrl: '../views/dialog-confirm.html',
  styleUrls: ['../../assets/css/dialog-alert.css']
})
export class DialogConfirmComponent {

  	constructor(
		public dialogRef: MatDialogRef<DialogConfirmComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	btnCancelClick(): void {
		this.dialogRef.close();
	}

}