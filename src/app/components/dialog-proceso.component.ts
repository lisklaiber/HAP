import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSelectModule } from '@angular/material';
// import { Rol } from '../models/rol';



@Component({
  selector: 'dialog-proceso',
  templateUrl: '../views/dialog-proceso.html',
  styleUrls: ['../../assets/css/dialog-proceso.css']
})
export class DialogPeocesoComponent {

    minDate = new Date(2000, 0, 1);
    maxDate = new Date(2022, 0, 1);

    public roles: any[];
    public usuarios: any[];
    public selects: any;

    constructor(
        public dialogRef: MatDialogRef<DialogPeocesoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        // console.log('data', this.data)
        this.roles = [];
        this.usuarios = [];
        this.selects = [];

        this.roles.push({id: 1, nombre: 'rol 1'});
        this.roles.push({id: 2, nombre: 'rol 2'});
        this.roles.push({id: 3, nombre: 'rol 3'});
        this.usuarios.push({id: 1, nombre: 'usuario 1'});
        this.usuarios.push({id: 2, nombre: 'usuario 2'});
        this.usuarios.push({id: 3, nombre: 'usuario 3'});

        this.selects.push({idRol: null, idUsuario: null, disable: true});
    }

	onNoClick(): void {
		this.dialogRef.close({procesoChanged: false});
	}

	BtnAddResponsableClick(){
		this.selects.push({idRol: null, idUsuario: null, disable: true});
	}

}
