import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSelectModule } from '@angular/material';
// import { Rol } from '../models/rol';



@Component({
  selector: 'dialog-ramas',
  templateUrl: '../views/dialog-ramas.html',
  styleUrls: []
})
export class DialogRamasComponent {

    constructor(
        public dialogRef: MatDialogRef<DialogRamasComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        // console.log('data', this.data)
    }

  onNoClick(): void {
    this.dialogRef.close({procesoChanged: false});
  }

}
