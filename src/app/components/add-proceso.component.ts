import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/*componentes*/
import { StructProcComponent } from './estructura-proceso.component';

/*Services*/
import { AddProcesoService } from '../services/add-proceso.service';
import { PlantillaService } from '../services/plantilla.service';

/*Models*/
import { Proceso } from '../models/proceso';
import { MacroProceso } from '../models/macro-proceso';
import { Template } from '../models/plantilla';

/*PopUp's*/
import { DialogAlert } from './dialog-alert';
import { ProcesoService } from '../services/proceso.service';
import { IndicadorComp, IndicadorTC } from '../models/indicador';
import { IndicadorService } from '../services/indicadores.service';
import { Estrategia } from '../models/estrategia';
import { EstrategiaService } from '../services/estrategia.service';

@Component({
	selector: 'add-proceso',
	templateUrl: '../views/add-proceso.html',
	styleUrls: ['../../assets/css/add-plantilla.css', 
		'../../assets/css/flujo.css', 
		'../../assets/css/add-user.css', 
		'../../assets/css/edit-user.css',
		'../../assets/css/add-proceso.css'],
	providers: [AddProcesoService, PlantillaService, IndicadorService, EstrategiaService]
})

export class AddProcesoComponent implements OnInit{
	public paso: number;
	public errorMessage: any;
	public templateSelected: any;
	private procesos: Proceso[];
	private levels: any;
	private serials: any;
	public macroProceso: Proceso;
	private idEmpresa: number;
	public templates: Template[];
	@ViewChild(StructProcComponent) StructProceso;
	public procesoBase;

	constructor(
		private _AddProcesoService: AddProcesoService,
		private _route: ActivatedRoute,
		private _router: Router,
		public dialog: MatDialog,
		private _PlantillaService: PlantillaService,
		private _ProcesoService: ProcesoService,
		private _IndicadorService: IndicadorService,
		private _EstraegiaService: EstrategiaService 
	){
		this.procesos = [];
		this.levels = [];
		this.serials = [];
		this.paso = 1;
		this.macroProceso = new Proceso(0, '', '', new Date(), new Date(), 0, true, 1, 0, 'Descripcion...',
			0, 1, 0);
		this.idEmpresa = 1;
		this.templateSelected = 0;
		this.procesoBase = {objeto: this.macroProceso, idProceso: null, tipo: 1};
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}

	Avanzar(){
		this.paso++;
	}

	ValidarProceso(){

		if(this.macroProceso.nombre === ''){
			this.openDialog("Debe ingresar un nombre");
		}else{
			this._AddProcesoService.ValidarProceso(this.macroProceso.nombre, this.macroProceso.idEmpresa).subscribe(
				result => {
					if(result.resp == "Aprobado"){
						this.Avanzar();
						this.procesoBase = {objeto: this.macroProceso, idProceso: null, tipo: 1};
						this.AddProceso()
						this.LoadTemplates();
					}else{
						this.openDialog(result.resp);
					}
				},
				error => {
					this.errorMessage = <any>error;
					if(this.errorMessage != null){
						console.log(this.errorMessage);
						this.openDialog("Ocurrión un problema");
					}
				}
			);
		}
	
	}

	AddProceso(){
		this._ProcesoService.AddProceso(this.macroProceso, 1).subscribe(
			response =>{
				this.procesoBase.idProceso = response.resp[0];
				// console.log(this.procesoBase);
			}, 
			reject =>{
				this.errorMessage = <any>reject;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.openDialog("Ocurrión un problema");
				}
			}
		);
	}

	LoadTemplates(){
		this._PlantillaService.GetTemplates(this.idEmpresa).subscribe(
			result => {
				if(result.templates == 0){
					// this.openDialog("No se encontró ninguna plantilla creada");
				}else{
					this.templates = result.resp;
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.openDialog("Ocurrión un problema al cargar las plantillas");
				}
			}
		);
	}

	SelectTemplate(){
		// console.log(this.templateSelected);
		this.paso++;
		if(this.templateSelected > 0){
			this.getProceso(this.templateSelected);
		}
	}

	getProceso(idTemplate){
		console.log(idTemplate);
	}

	openDialog(texto): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto
		});

		// dialogRef.afterClosed().subscribe(result => {
		//   console.log('The dialog was closed');
		//   this.animal = result;
		// });
	}

	openDialogProc(texto: string): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto
		});

		dialogRef.afterClosed().subscribe(result => {
		  this.Avanzar();
		});
	}

	CrearIndicadores(){
		let idsProcesos = [];
		let last = false;
		this._ProcesoService.GetIdsProcesos(this.procesoBase.idProceso).subscribe(
			response => {
				idsProcesos = response.resp;
				for (let index = 0; index < idsProcesos.length; index++) {
					this.AddIndicador(idsProcesos[index].ID_Proceso, 1, last);
					this.AddIndicador(idsProcesos[index].ID_Proceso, 2, last);
					if(index == ( idsProcesos.length - 1 ))
						last = true
					this.AddIndicador(idsProcesos[index].ID_Proceso, 3, last);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.openDialog("Ocurrión un problema al crear el proceso, intentalo de nuevo");
				}
			}	
		);
	}

	AddIndicador(idProceso:number, tipo: number = 1, last: boolean){
		let indicador: any;

		if(tipo === 1){
			indicador = new IndicadorComp('% Completado', 0.0, 0.0, 0.0, 0.0, 0.0, '', idProceso);
		}else if(tipo === 2){
			indicador = new IndicadorTC('Tiempo', 1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, '', idProceso);
		}
		else{
			indicador = new IndicadorTC('Costo', 2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, '', idProceso);
			tipo = 2;
		}
		this._IndicadorService.AddIndicador(indicador, tipo).subscribe(
			response => {
				if(!response.resp){
					// this.openDialog("Ocurrión un problema al crear el proceso, intentalo de nuevo");
					console.log(response.resp);
				}
				else{
					if(tipo === 1){
						for (let index = 1; index <= 3; index++) {
							this.AddEstrategia(response.id, index, tipo, last);
						}
					}
					else{
						for (let index = 1; index <= 5; index++) {
							let lastEst = false;
							if(last === true && index === 5)
								lastEst = true;
							this.AddEstrategia(response.id, index, tipo, lastEst);
						}
					}
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					// this.openDialog("Ocurrión un problema al crear el proceso, intentalo de nuevo");
				}
			}
		);

	}

	AddEstrategia(idIndicador: number, semaforo: number, tipo: number, last:boolean){
		let estrategia = new Estrategia('', '', idIndicador, semaforo);

		this._EstraegiaService.AddEstrategia(estrategia, tipo).subscribe(
			response => {
					if(last === true){
						this.openDialogProc("Proceso Creado");
					}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					// this.openDialog("Ocurrión un problema al crear el proceso, intentalo de nuevo");
				}
			}
		);
	}

	btnCrearProcClick(){
		this.procesos = this.StructProceso.procesos;
		this.levels = this.StructProceso.levels;
		this.serials = this.StructProceso.serials;
		
		this.CrearIndicadores();

		// this.Avanzar();	
	}
	
	Retroceder(){
		this.paso--;
	}
}
