import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatSort, MatTableDataSource} from '@angular/material';


import { ProcesoViewService } from '../services/proceso-view.service';
import { ProcesoService } from '../services/proceso.service';
import { Serial } from '../models/serial';
import { serie } from '../models/serieChart';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'proceso-view',
	templateUrl: '../views/proceso-view.html',
	styleUrls: ['../../assets/css/Procesos.css', 
	'../../assets/css/proceso-view.css'],
	providers: [ ProcesoViewService ]
})

export class ProcesoViewComponent implements OnInit{
	@Input() proceso: any;
	public procesoPrinc: any[] = [];
	public procesos: any[] = [];
	public levels: any[] = [];
	public serials: any[] = [];
	private errorMessage: string;
	public limitSerial: boolean;
	public responsable: any;
	public pendientes: any[];
	public series: serie[] = [];
	

	displayedColumns = ['Tipo', 'Nombre', 'Fecha_Fin', 'Responsable'];
  	dataSource = new MatTableDataSource();

  	@ViewChild(MatSort) sort: MatSort;

	constructor(
		private _ProcesoViewService: ProcesoViewService,
		private _route: ActivatedRoute,
		private _router: Router,
		private _ProcesoService: ProcesoService
	){
		this.limitSerial = false;
		this.responsable = {};
		this.pendientes = [];
		this.series.push(new serie('Completado', '#dc304b', 67, '112%', '88%'));
		this.series.push(new serie('Tiempo', '#e2ca3b', 77, '87%', '63%'));
		this.series.push(new serie('Costo', '#1db28d', 92, '62%', '38%'));
	}

	GetPendientes(){
		this._ProcesoService.GetPendientes(this.proceso.ID_Proceso).subscribe(
			response =>{
				this.pendientes = response.resp;
				console.log('pendientes', this.pendientes);
				// console.log('data', ELEMENT_DATA);
				this.dataSource = new MatTableDataSource(this.pendientes);
				this.dataSource.sort = this.sort;
				// console.log('dataSource', this.dataSource);
				// for(let pendiente of this.pendientes)
				// 	G
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	GetProcPrin(){
		this.GetHijos();
	}

	GetHijos(){
		this._ProcesoService.GetHijos(this.proceso.ID_Proceso).subscribe(
			result => {
				if(result.resp.length == 1){
					if(result.resp[0].Tipo_Hijo == 1){
						this.GetProceso(result.resp[0].Hijo);
					}
					else if (result.resp[0].Tipo_Hijo == 2){
						this.GetEvento(result.resp[0].Hijo);
					}
					else
						this.GetDecision(result.resp[0].Hijo);
				}
				else{
					for(let level of result.resp ){
						this.GetSerials(level.Hijo, level.Tipo_Hijo);
					}
				}
				console.log('ProcPrinc', this.procesoPrinc);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	GetSerials( idProceso, tipo){
		this._ProcesoService.GetSerials(idProceso, tipo).subscribe(
			result => {
				if(result.resp.length == 1 && this.limitSerial == false){
					if(result.resp[0].Tipo_Sub_Proceso == 1){
						this.GetProceso(result.resp[0].ID_Sub_Proceso);
					}
					else if (result.resp[0].Tipo_Sub_Proceso == 2){
						this.GetEvento(result.resp[0].ID_Sub_Proceso);
					}
					else
						this.GetDecision(result.resp[0].ID_Sub_Proceso);
				}
				else if (this.limitSerial == false) {
					this.limitSerial = true;
					if(result.resp[0].Tipo_Sub_Proceso == 1){
						this.GetProceso(result.resp[0].ID_Sub_Proceso);
					}
					else if (result.resp[0].Tipo_Sub_Proceso == 2){
						this.GetEvento(result.resp[0].ID_Sub_Proceso);
					}
					else
						this.GetDecision(result.resp[0].ID_Sub_Proceso);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	GetProceso(idProceso){
		this._ProcesoService.GetProceso(idProceso).subscribe(
			result => {
				this.procesoPrinc.push(result.resp);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);

	}

	GetEvento(idEvento){
		this._ProcesoService.GetEvento(idEvento).subscribe(
			result => {
				this.procesoPrinc.push(result.resp);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	GetDecision(idDecision){
		this._ProcesoService.GetDecision(idDecision).subscribe(
			result => {
				this.procesoPrinc.push(result.resp);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	GetResponsable(){
		this._ProcesoService.GetResponsable(this.proceso.ID_Proceso).subscribe(
			result => {
				this.responsable = result.resp;
				console.log('responsable', this.responsable)
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}

	ngOnInit(){
		this.GetProcPrin();
		this.GetResponsable();
		this.GetPendientes();
	}
	
}
// export interface PeriodicElement {
// 	Icono: number;
// 	Nombre: string;
// 	['Fecha Termino']: number;
// 	Respobsable: string;
//   }
  
//   let ELEMENT_DATA: PeriodicElement[] = [
// 	{Icono: 1, Nombre: 'Hydrogen', ['Fecha Termino']: 1.0079, Respobsable: 'H'},
// 	{Icono: 2, Nombre: 'Helium', ['Fecha Termino']: 4.0026, Respobsable: 'He'},
// 	{Icono: 3, Nombre: 'Lithium', ['Fecha Termino']: 6.941, Respobsable: 'Li'},
// 	{Icono: 4, Nombre: 'Beryllium', ['Fecha Termino']: 9.0122, Respobsable: 'Be'},
// 	{Icono: 5, Nombre: 'Boron', ['Fecha Termino']: 10.811, Respobsable: 'B'},
// 	{Icono: 6, Nombre: 'Carbon', ['Fecha Termino']: 12.0107, Respobsable: 'C'},
// 	{Icono: 7, Nombre: 'Nitrogen', ['Fecha Termino']: 14.0067, Respobsable: 'N'},
// 	{Icono: 8, Nombre: 'Oxygen', ['Fecha Termino']: 15.9994, Respobsable: 'O'},
// 	{Icono: 9, Nombre: 'Fluorine', ['Fecha Termino']: 18.9984, Respobsable: 'F'},
// 	{Icono: 10, Nombre: 'Neon', ['Fecha Termino']: 20.1797, Respobsable: 'Ne'}
//   ];