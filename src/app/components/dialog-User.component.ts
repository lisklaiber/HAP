import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSelectModule,} from '@angular/material';
import { User } from '../models/user';



@Component({
  selector: 'dialog-User',
  templateUrl: '../views/dialog-User.html'
})
export class DialogUserComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  		if(!data.user){
  			data.user = new User(1,"","","","","","","")
  		}
    }

  onNoClick(): void {
    this.dialogRef.close({userChanged: false});
  }

}