import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RolService } from '../services/rol.service';

/**
 * @title List with selection
 */
@Component({
  selector: 'dialog-permisos-rol',
  styleUrls: [],
  templateUrl: '../views/dialog-hijos.html'
})
export class DialogPermisosRolComponent {
  public hijos: any[];
  public errorMessage: any;
  public permisosSelected: any;
  public hijosSelected: any;
  constructor(
    private _RolService: RolService,
    public dialogRef: MatDialogRef<DialogPermisosRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ){
    this.hijos = [];
  }

  ngOnInit(): void {
    this.hijos = [];
    this.GetPermisos();
   
  }

  onNoClick(): void {
    this.dialogRef.close({serialChanged: false});
  }

  GetPermisos(){
    // console.log(this.data.idRol);
    this._RolService.GetPermisos(this.data.idRol).subscribe(
      result => {
        this.hijos = [];
        this.hijos = result.resp;
      },
      error => {
        this.errorMessage = <any>error;
        if(this.errorMessage != null){
          console.log(this.errorMessage);
        }
      }
    );
  }

  onAreaListControlChanged(list){
    this.hijosSelected = list.selectedOptions.selected.map(item => item.value);
  }

}