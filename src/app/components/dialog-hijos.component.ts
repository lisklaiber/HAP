 import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProcesoService } from '../services/proceso.service';

/**
 * @title List with selection
 */
@Component({
	selector: 'dialog-hijos',
	styleUrls: [],
	templateUrl: '../views/dialog-hijos.html'
})
export class DialogHijosComponent {
	public hijos: any[];
	public errorMessage: any;
	public hijosSelected: any;
	constructor(
		public _ProcesoService: ProcesoService,
		public dialogRef: MatDialogRef<DialogHijosComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	){
		this.hijos = [];
	}

	ngOnInit(): void {
		this.hijos = [];
		this.GetPredecesores();
	}

	onNoClick(): void {
		this.dialogRef.close({serialChanged: false});
	}

	GetPredecesores(){
		this._ProcesoService.GetHijos(this.data.idPadre).subscribe(
			result => {
				this.hijos = [];
				// console.log("levels", result.resp);
				for(let level of result.resp ){
					if(this.data.idProceso != level.Hijo)
						this.GetHijos(level.Hijo, level.Tipo_Hijo);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	
	
	GetHijos(hijo: number, tipo: number){
			if(tipo == 1){
					this.GetProceso(hijo);
			} else if (tipo == 2){
					this.GetEvento(hijo);
			} else{
					this.GetDecision(hijo);
			}
	}
	
	GetProceso(idProceso){
		this._ProcesoService.GetProceso(idProceso).subscribe(
			result => {
				this.hijos.push(result.resp);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	
	GetEvento(idEvento){
		this._ProcesoService.GetEvento(idEvento).subscribe(
			result => {
				this.hijos.push(result.resp);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	
	GetDecision(idDecision){
		this._ProcesoService.GetDecision(idDecision).subscribe(
			result => {
				this.hijos.push(result.resp);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
				}
			}
		);
	}
	onAreaListControlChanged(list){
		this.hijosSelected = list.selectedOptions.selected.map(item => item.value);
	}

}