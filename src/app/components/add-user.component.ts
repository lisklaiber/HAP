import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Response, Headers } from '@angular/http';
import { ConfigAPI } from '../models/ApiConfig';
/*PopUp's*/
import { DialogAlert } from './dialog-alert';
import { DialogConfirmComponent } from './dialog-confirm.component';
/*Services*/
import { UserService } from '../services/user.service';

/* Models */ 
import {User} from '../models/user';



@Component({
	selector: 'add-user',
	templateUrl: '../views/add-user.html',
	styleUrls: ['../../assets/css/add-user.css',
	 '../../assets/css/flujo.css'],
	providers: [UserService]
})

export class AddUserComponent implements OnInit{
	public paso: number;
	private errorMessage: any;
	public usuario: User;
	private _configApi: ConfigAPI;
	public foto: any;

	constructor(
		private _UserService: UserService,
		private _route: ActivatedRoute,
		private _router: Router,
		public dialog: MatDialog,
		private _Http: Http,
	){
		this._configApi = new ConfigAPI();
		this.paso = 1;
		this.usuario= new User(1,'','','','','','','');
		this.foto = "";

	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}

	validarCorreo(){
		this._UserService.validarCorreo(this.usuario.correo).subscribe(
			response =>{
				if(response.resp){
					// console.log(response.resp);
					this.openDialog(response.resp);
				}
				else{
					console.log("error");
				}
			},
			error =>{
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					// console.log(this.errorMessage);
					// alert("Usuario no encontrado");
				}
			}
		);

	}

	openDialog(texto): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto//{ name: this.name, animal: this.animal }
		});

		dialogRef.afterClosed().subscribe(result => {
			if(texto === "Aprobado"){
				this.Avanzar();
			}
			else{
				this.usuario.correo = "";
				// console.log('The dialog was closed');
				// this.animal = result;
			}
		});	
	}

	Avanzar(){
		this.paso++;
	}
	
	Retroceder(){
		this.paso--;
	}

	AddUser(){
		this.usuario.foto = this.foto;
		let formData:FormData = new FormData();
		formData.append('correo', this.usuario.correo);
		formData.append('nombre', this.usuario.nombre);
		formData.append('apPaterno', this.usuario.apPaterno);
		formData.append('apMaterno', this.usuario.apMaterno);
		formData.append('pass', this.usuario.pass);
		formData.append('color', this.usuario.color);
		formData.append('idEmpresa', `${this.usuario.idEmpresa}`);
		formData.append('foto', this.foto);
		let opt ={content: formData}
		this._UserService.AddUser(formData).subscribe(
			response => {
				this.openDialog("Usuario agregado correctamente");
				this.Avanzar();
				console.log(response);
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					this.openDialog("Ups! Ocurrió un error");
					console.log(this.errorMessage);
				}
			}
		);
	}

	fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        this.foto = fileList[0];
    }
    else{
    	this.foto = "";
    	}
	}

	
}
