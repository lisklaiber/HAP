import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ProcesoService } from '../services/proceso.service';

//import { Autentication } from '../models/autentication';

@Component({
	selector: 'proceso-home',
	templateUrl: '../views/proceso-home.html',
	styleUrls: ['../../assets/css/Procesos.css'],
	providers: [
		ProcesoService
	]
})

export class ProcesoHomeComponent implements OnInit{
	public logeado:boolean;
	public procesos: number[] = [];
	public errorMessage: string;
	constructor(
		private _ProcesoService: ProcesoService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		this.logeado = true;
	}

	GetProcesos(){
		this._ProcesoService.GetProcesosBase(1).subscribe(
			result => {
				// console.log('result', result);
				if(!result.existe){
					alert(result.resp);
				}else{
					this.procesos = result.resp;
					// console.log(this.procesos);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					alert('Error en la peticion');
				}
			}
		);
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#F0F3F7";
		this.GetProcesos();

	}

	
	
}
