// chart.component.ts
import { Component, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';

import solidgauge from 'highcharts/modules/solid-gauge';
import * as HighchartsMore  from 'highcharts/highcharts-more.src';
import * as Highcharts from 'highcharts';
HighchartsMore(Highcharts);
solidgauge(Highcharts);

@Component({
	selector: 'chart-circle',
	templateUrl: '../views/chart.html',
	styleUrls: [],
	providers: []
})

export class ChartComponent {
    public chart: Chart;
    private title: string;
    @Input() series: any[];

    constructor(){

        this.series = [{
            name: 'Completado',
            data: [{
                color: '#dc304b',
                radius: '112%',
                innerRadius: '88%',
                y: 0
            }]
        }, {
            name: 'Tiempo',
            data: [{
                color: '#e2ca3b',
                radius: '87%',
                innerRadius: '63%',
                y: 0
            }]
        }, {
            name: 'Costo',
            data: [{
                color: '#1db28d',
                radius: '62%',
                innerRadius: '38%',
                y: 0
            }]
        }];

        this.chart = new Chart({
            chart: {
                type: 'solidgauge'
            },
        
            title: {
                text: null
                // style: {
                //     fontSize: '24px'
                // }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                borderWidth: 0,
                backgroundColor: 'none',
                shadow: false,
                style: {
                    fontSize: '16px'
                },
                pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
                positioner: function (labelWidth) {
                    return {
                        x: (this.chart.chartWidth - labelWidth) / 2,
                        y: (this.chart.plotHeight / 2) - 25
                    };
                }
            },
            pane: {
                startAngle: 0,
                endAngle: 360,
                background: [{ // Track for Move
                    outerRadius: '112%',
                    innerRadius: '88%',
                    backgroundColor: '#CCC',
                    borderWidth: 0
                }, { // Track for Exercise
                    outerRadius: '87%',
                    innerRadius: '63%',
                    backgroundColor: '#CCC',
                    borderWidth: 0
                }, { // Track for Stand
                    outerRadius: '62%',
                    innerRadius: '38%',
                    backgroundColor: '#CCC',
                    borderWidth: 0
                }]
            },
            yAxis: {
                min: 0,
                max: 100,
                lineWidth: 0,
                tickPositions: []
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        enabled: false
                    },
                    linecap: 'round',
                    stickyTracking: false,
                    rounded: true
                }
            },
            series: this.series
        });
    }

    ngOnInit(): void {
        this.chart.options.series = this.series;
        
        if(this.series.length == 1){
            this.chart.options.pane.background= [{ // Track for Move
                outerRadius: '112%',
                innerRadius: '88%',
                backgroundColor: '#CCC',
                borderWidth: 0
            }]
            this.chart.options.chart.height = '300px';
        }
    }
    
    // add point to chart serie
    // add() {
    //     this.chart.addPoint(Math.floor(Math.random() * 10));
    // }
}