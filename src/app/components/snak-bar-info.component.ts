import { Component, Inject } from "@angular/core";
import { MAT_SNACK_BAR_DATA } from "@angular/material";

@Component({
    selector: 'snak-bar',
    templateUrl:'../views/snak-bar-info.html',
    styles:[`
        .snakInfo{
            text-aling: center;
            color: #e2ca3b;
        }
    `]
})
export class SnakBarInfoComponent {
    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any){

    }
}