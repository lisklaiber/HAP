import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { serie } from '../models/serieChart';



//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'inidicadores',
	templateUrl: '../views/indicadores.html',
	styleUrls: ['../../assets/css/indicadores.css', '../../assets/css/Procesos.css'],
	providers: []
})

export class IndicadoresComponent implements OnInit{
	public series: serie[] = [];	
	constructor(
	
	){
		this.series.push(new serie('Completado', '#dc304b', 67, '112%', '88%'));
		this.series.push(new serie('Desciación', '#e2ca3b', 77, '112%', '88%'));	
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}
	
}
