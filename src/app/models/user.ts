export class User{
	constructor (
		public idEmpresa: number,
		public correo: string,
		public pass: string,
		public nombre: string,
		public apPaterno: string,
		public apMaterno: string,
		public foto: any,
		public color: string,
	){}
}