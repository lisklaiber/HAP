export class serie {
    public name: string;
    public data: Array<Object>;
    
    constructor(name: string, color: string, value: number, radius:string, innerRadius:string ){
        this.name = name,
        this.data = [{
            color: color,
            radius: radius,
            innerRadius: innerRadius,
            y: value
        }]
    }
}