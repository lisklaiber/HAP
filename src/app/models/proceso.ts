export class Proceso{
	constructor(
		public subID: number,
		public nombre: string,
		public abreviatura: string,
		public fechaIni: Date,
		public fechaFin: Date,
		public duracion: number,
		public obligatorio: boolean,
		public tipo: number,
		public completado: number,
		public descripcion: string,
		public costo: number,
	    public idEmpresa: number,
		public idProcBase: number
	){}
}