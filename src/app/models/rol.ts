export class Rol{
	constructor (
		public descripcion: string,
		public nombre: string,
		public color: string,
		public icono: string,
		public idEmpresa: number
	){}
}