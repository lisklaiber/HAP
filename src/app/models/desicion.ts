export class Desicion{
    constructor(
        public tipo: number,
        public nombre: string,
        public descripcion: string,
        public condicion: string
    ){}
}