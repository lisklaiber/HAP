export class Estrategia {
    constructor(
        public nombre: string, 
        public descripcion: string, 
        public idIndicador: number, 
        public semaforo: number
    ){}
}