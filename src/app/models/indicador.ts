export class Indicador{
    constructor(
        public nombre: string,
        public verdeMax: number,
        public verdeMin: number,
        public descripcion: string,
        public idProceso: number
    ){
    }
}
export class IndicadorComp extends Indicador{
    constructor(
        public nombre: string,
        public verdeMax: number,
        public verdeMin: number,
        public amMax: number,
        public amMin: number,
        public rojo: number,
        public descripcion: string,
        public idProceso: number
    )
    {
        super(nombre, verdeMax, verdeMin, descripcion, idProceso);
    }
}

export class IndicadorTC extends Indicador{
    constructor(
        public nombre: string,
        public tipo: number,
        public verdeMax: number,
        public verdeMin: number,
        public amSupMax: number,
        public amSupMin: number,
        public amInfMax: number,
        public amInfMin: number,
        public rojoSup: number,
        public rojoInf: number,
        public descripcion: string,
        public idProceso: number
    ){
        super(nombre, verdeMax, verdeMin, descripcion, idProceso);
    }
}