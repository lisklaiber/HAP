export class Level{
    constructor(
        public idProceso: number, 
        public padre: number, 
        public hijo: number, 
        public tipoHijo: number
    ){}
}