export class Insumo{
    constructor(
        public nombre: string, 
        public fechaEntrega: Date, 
        public obligatorio: boolean, 
        public tipo: number, 
        public completado: number, 
        public descripcion: string, 
        public archivo: string, 
        public costo: number
    ){}
}