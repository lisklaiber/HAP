export class Serial{
    constructor (
        public idProceso: number, 
        public idSubProceso: number, 
        public idSeriado: number, 
        public tipoSubProceso: number, 
        public tipoSeriado: number
    ){
    }
}