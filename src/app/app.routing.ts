import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login.component';
import { ProcesoHomeComponent } from './components/proceso-home.component';
import { DetalleComponent } from './components/detalle.component';
import { IndicadoresComponent } from './components/indicadores.component';
import { AddUserComponent } from './components/add-user.component';
import { ListUsersComponent } from './components/list-users.component';
import { EditUserComponent } from './components/edit-user.component';
import { AddPlantillaComponent } from './components/add-plantilla.component';
import { AddProcesoComponent } from './components/add-proceso.component';
import { EditProcesoComponent} from './components/edit-proceso.component';
import { EditPlantillaComponent } from './components/edit-planitlla.component';
import { RolComponent } from './components/rol.component';

const appRoutes: Routes = [
	{path: '', component: ProcesoHomeComponent},
	{path: 'login', component: ProcesoHomeComponent},
	{path: 'proceso', component: ProcesoHomeComponent},
	{path: 'detalle', component: DetalleComponent},
	{path: 'indicadores', component: IndicadoresComponent},
	{path: 'admin/user', component: AddUserComponent},
	{path: 'admin/list-user', component: ListUsersComponent},
	{path: 'admin/edit-user', component: EditUserComponent},
	{path: 'admin/plantilla', component: AddPlantillaComponent},
	{path: 'admin/edit-plantilla', component: EditPlantillaComponent},
	{path: 'admin/proceso', component: AddProcesoComponent},
	{path: 'admin/edit-proceso', component: EditProcesoComponent},
	{path: 'admin/rol', component: RolComponent},
	{path: '**', component: ProcesoHomeComponent}
];

	export const appRoutingProviders: any[] = [];
	export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);